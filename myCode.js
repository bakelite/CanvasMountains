//Name
var firstName = "Johan";
var lastName = "Hellgren";
var myName = document.getElementById("name");
myName.innerHTML = firstName + " " + lastName;
//Get canvas and content
var grabCan = document.querySelector("canvas");
var can = grabCan.getContext("2d");
//Width and height of mountains
var mountainWidth = 100;
var mountainHeight = 45;
//Start height, i.e where the mountains should begin and how much of the sky should be visible
var startY = 200;
//Set sky color
can.fillStyle = "rgb(239, 137, 119)";
can.fillRect(0, 0, grabCan.width, grabCan.height);
makeSun();

for(var i = 0; i < 7; i++)
{
    makeMountains();
    //Move start height "up"
    startY += 50;
}

//Function for interpolation
function lerp(start, end, alpha)
{
    return start * (1 - alpha) + end * alpha;
}

function makeSun()
{
    can.beginPath();
    can.save();
    //Set sun size
    can.arc(200, 200, 100, 0, 2 * Math.PI);
    can.lineWidth = 100;
    //Combine fill and stroke for a simple two-tone sun
    can.strokeStyle = "rgb(255, 123, 84)";
    can.fillStyle = "rgb(255, 178, 84)";
    //Fake glow by using shadow blur
    can.shadowBlur = 1000;
    can.shadowColor = "rgb(255, 178, 84)";
    //Draw and restore settings to prevent shadow blur from being applied to everything
    can.stroke();
    can.fill();
    can.restore();
}

function makeMountains()
{
    can.beginPath();
    //Coordinates
    var x = 0;
    var y = 0;
    //Variables for helping determine average height
    var everyY = [];
    var averageY = 0;
    //Create lines as long as x is less than width. Added 100 to make sure that the last x is added even if it falls slightly outside the frame
    while(x < grabCan.width + 100)
    {
        //Set y to random in range
        y = Math.random() * ((startY + mountainHeight) - (startY - mountainHeight)) + (startY - mountainHeight);
        //Add to y array
        everyY.push(y);
        //Draw lines
        can.lineTo(x, y);
        //Set x to random in range
        x += Math.random() * (mountainWidth - mountainWidth) + mountainWidth;
    }
    //Calculate average height of mountain range in order to accurately interpolate color, basing it on startY will set wrong color if the range is significantly below or above startY
    for(var i = 0; i < everyY.length; i++)
    {
        averageY += everyY[i];
    }
    averageY = averageY / everyY.length;
    //Interpolate color
    var alpha = lerp(1, 0, 1 - (grabCan.height - averageY) / grabCan.height);
    console.log(alpha);
    var r = Math.round(lerp("150", "252", alpha));
    var g = Math.round(lerp("20", "200", alpha));
    var b = Math.round(lerp("100", "100", alpha));
    //Make final color
    var finalColor = "rgb(" + r + "," + g + "," + b + ")";
    can.fillStyle = finalColor;
    //Close the shape and draw
    can.lineTo(grabCan.width, grabCan.height);
    can.lineTo(0, grabCan.height);
    can.fill();
}